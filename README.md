# crazy-cloud

#### 项目介绍
`crazy-cloud`提供统一认证中心，支持分布式部署和线性拓展，业务模块可直接在此基础上直接新建项目拓展服务。核心技术采用Eureka、Fegin、Ribbon、Zuul、Hystrix、security和oauth2等主要框架和中间件，
基于SpringBoot和SpringCloud的项目建设，可直接应用于生产环境中。建设此项目的目的是个人兴趣，并把自己在项目使用和学习的东西做一个总结，并且分享给大家，减少开发踩坑。后续服务会不断升级和维护，欢迎大家一起探讨和学习。

#### 项目架构
1、crazy-cloud-common 公共方法封装，所有项目可引用<br/>
2、crazy-cloud-admin 给予eureka注册中心的系统监控中心<br/>
3、crazy-cloud-eureka 基于eureka的注册服务中心<br/>
4、crazy-cloud-zuul网关入口目前版本是zuul后续会考虑升级为gateway<br/>
5、crazy-cloud-guid 全局唯一id生成器，支持分布式部署<br/>
6、crazy-cloud-security 用户注册登录认证中心等，Srping security oAuth 提供单点登录接口，方便其他系统对接可以支持token，code等模式<br/>
7、crazy-cloud-user 封装用户为服务，为用户注册，登录、获取用户信息等提供接口服务-只提供服务，不对外暴露<br/>
#### 整体架构图
![整体架构图](https://images.gitee.com/uploads/images/2018/1004/215444_04ed5d2d_1448662.png "280044-20171115113828234-120923910.png")
#### 功能截图
注册中心eureka
![eureka](https://images.gitee.com/uploads/images/2018/1004/214433_b42df43b_1448662.png "WX20181004-214404@2x.png")
监控中心admin
![admin2.0](https://images.gitee.com/uploads/images/2018/1004/214734_f1f28897_1448662.png "WX20181004-214553@2x.png")
![admin2.0](https://images.gitee.com/uploads/images/2018/1004/214756_6fa51182_1448662.png "WX20181004-214537@2x.png")
![admin2.0](https://images.gitee.com/uploads/images/2018/1004/214808_682e4430_1448662.png "WX20181004-214700@2x.png")

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)tee-stars/](https://gitee.com/gitee-stars/)