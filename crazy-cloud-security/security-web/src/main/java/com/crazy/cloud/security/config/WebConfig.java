package com.crazy.cloud.security.config;

import com.crazy.cloud.common.xss.XssDefenseFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @program: crazy-cloud
 * @description: WebConfig
 * @author: CrazyMan
 * @create: 2018/10/10 上午11:15
 **/
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    @Bean
    public FilterRegistrationBean xssDefenseFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new XssDefenseFilter());
        return registrationBean;
    }


}
