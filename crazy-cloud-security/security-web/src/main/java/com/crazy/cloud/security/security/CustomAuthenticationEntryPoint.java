package com.crazy.cloud.security.security;

import com.alibaba.fastjson.JSON;
import com.crazy.cloud.common.api.ApiResponse;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * @program: crazy-cloud
 * @description: CustomAuthenticationEntryPoint
 * @author: CrazyMan
 * @create: 2018/10/12 下午5:37
 **/
@Slf4j
@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException ae) throws IOException {
        log.info("Pre-authenticated entry point called. Rejecting access");
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(JSON.toJSONString(ApiResponse.error(40401, "身份认证失效,请重新登录")));
    }
}
