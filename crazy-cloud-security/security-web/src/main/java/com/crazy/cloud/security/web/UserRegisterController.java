package com.crazy.cloud.security.web;

import com.crazy.cloud.common.api.ApiResponse;
import com.crazy.cloud.common.exception.DataConflictException;
import com.crazy.cloud.security.biz.dto.UserRegisterRequestDTO;
import com.crazy.cloud.security.biz.service.UserInfoService;
import com.crazy.cloud.security.biz.service.UserRegisterService;
import com.crazy.cloud.user.api.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: crazy-cloud
 * @description: 用户注册controller
 * @author: CrazyMan
 * @create: 2018/9/28 上午11:07
 **/
@Slf4j
@RestController
@RequestMapping("/users/register")
public class UserRegisterController {

    @Autowired
    UserInfoService userInfoService;
    @Autowired
    UserRegisterService userRegisterService;

    /**
     * 用户名检查
     *
     * @param username 用户名
     */
    @GetMapping("/check/username/{username}")
    public ApiResponse checkUsername(@PathVariable String username) {
        UserDTO userDTO = userInfoService.getUserByUsername(username);
        if (null != userDTO) {
            throw new DataConflictException("用户名已存在");
        }
        return ApiResponse.success();
    }

    /**
     * 电话检查
     *
     * @param phoneNo 电话
     */
    @GetMapping("/check/phone/{phoneNo}")
    public ApiResponse checkPhoneNo(@PathVariable String phoneNo) {
        UserDTO userDTO = userInfoService.getUserByPhoneNo(phoneNo);
        if (null != userDTO) {
            throw new DataConflictException("手机号已存在");
        }
        return ApiResponse.success();
    }

    /**
     * email检查
     *
     * @param email 邮箱
     */
    @GetMapping("/check/email/{email}")
    public ApiResponse checkEmail(@PathVariable String email) {
        UserDTO userDTO = userInfoService.getUserByEmail(email);
        if (null != userDTO) {
            throw new DataConflictException("邮箱已存在");
        }
        return ApiResponse.success();
    }

    /**
     * 注册
     *
     * @param userRegisterRequestDTO 注册信息
     */
    @PostMapping
    public ApiResponse register(@RequestBody UserRegisterRequestDTO userRegisterRequestDTO) {
        return new ApiResponse(userRegisterService.register(userRegisterRequestDTO));
    }

}
