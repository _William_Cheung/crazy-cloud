package com.crazy.cloud.security.biz.service.impl;

import com.crazy.cloud.common.exception.DataConflictException;
import com.crazy.cloud.common.utils.ConvertUtils;
import com.crazy.cloud.security.biz.dto.UserRegisterRequestDTO;
import com.crazy.cloud.security.biz.service.UserRegisterService;
import com.crazy.cloud.user.api.dto.UserRegisterDTO;
import com.crazy.cloud.user.api.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: crazy-cloud
 * @description: 注册
 * @author: CrazyMan
 * @create: 2018/9/28 下午3:43
 **/
@Slf4j
@Service
public class UserRegisterServiceImpl implements UserRegisterService {

    @Autowired
    UserService userService;

    @Override
    public String register(UserRegisterRequestDTO userRegisterRequestDTO) {
        UserRegisterDTO userRegisterDTO = ConvertUtils.convert(userRegisterRequestDTO, UserRegisterDTO.class);
        if (null != userService.getUserInfoByPhoneNo(userRegisterDTO.getPhoneNo())) {
            throw new DataConflictException("手机已注册");
        }
        if (null != userService.getUserInfoByEmail(userRegisterDTO.getEmail())) {
            throw new DataConflictException("邮箱已经被注册使用");
        }
        if (null != userService.getUserInfoByUsername(userRegisterDTO.getUsername())) {
            throw new DataConflictException("用户名已经被注册使用");
        }
        return userService.userRegister(userRegisterDTO);
    }


}
