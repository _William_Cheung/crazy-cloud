package com.crazy.cloud.security.biz.service.impl;

import com.crazy.cloud.security.biz.service.UserInfoService;
import com.crazy.cloud.user.api.dto.UserDTO;
import com.crazy.cloud.user.api.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: crazy-cloud
 * @description: 用户登录实现
 * @author: CrazyMan
 * @create: 2018/9/27 下午5:32
 **/
@Slf4j
@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    UserService userService;

    /**
     * 根据clientId查询
     *
     * @param clientId 用户唯一ID
     */
    @Override
    public UserDTO getUserByClient(String clientId) {
        return userService.getUserInfoByClientId(clientId);
    }

    /**
     * 根据phoneNo查询
     *
     * @param phoneNo 电话号码
     */
    @Override
    public UserDTO getUserByPhoneNo(String phoneNo) {
        return userService.getUserInfoByPhoneNo(phoneNo);
    }

    /**
     * 根据email查询
     *
     * @param email 电话号码
     */
    @Override
    public UserDTO getUserByEmail(String email) {
        return userService.getUserInfoByEmail(email);
    }

    /**
     * 根据username查询
     *
     * @paramusername 电话号码
     */
    @Override
    public UserDTO getUserByUsername(String username) {
        return userService.getUserInfoByUsername(username);
    }
}
