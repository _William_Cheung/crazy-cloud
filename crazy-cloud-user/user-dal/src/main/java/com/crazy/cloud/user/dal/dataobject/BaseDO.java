package com.crazy.cloud.user.dal.dataobject;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import lombok.Data;

/**
 * @program: crazy-cloud
 * @description: 基础DO类
 * @author: CrazyMan
 * @create: 2018/9/25 下午9:11
 **/
@Data
public class BaseDO {

    @TableField(fill = FieldFill.INSERT)
    private String creator;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifier;
    @TableId(type = IdType.AUTO)
    private Long id;
    private Date created;
    @TableField(fill = FieldFill.UPDATE)
    private Date modified;
    @TableLogic
    private Integer flag;

}
