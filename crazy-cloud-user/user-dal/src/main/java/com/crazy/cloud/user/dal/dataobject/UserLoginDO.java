package com.crazy.cloud.user.dal.dataobject;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: crazy-cloud
 * @description: 用户登录DO
 * @author: CrazyMan
 * @create: 2018/9/26 下午4:05
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_login")
public class UserLoginDO extends BaseDO {

    private String clientId;

    private String passwordEncryption;

    private String salt;

    private String status;

}
