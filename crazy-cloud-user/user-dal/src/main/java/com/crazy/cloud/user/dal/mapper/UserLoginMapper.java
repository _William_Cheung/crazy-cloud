package com.crazy.cloud.user.dal.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.crazy.cloud.user.dal.dataobject.UserLoginDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: crazy-cloud
 * @description: 用户登录mapper
 * @author: CrazyMan
 * @create: 2018/9/26 下午4:06
 **/
@Mapper
public interface UserLoginMapper extends BaseMapper<UserLoginDO> {

}
