package com.crazy.cloud.user.api.dto;

import java.util.Date;
import lombok.Data;

/**
 * @program: crazy-cloud
 * @description: 用户DTO
 * @author: CrazyMan
 * @create: 2018/9/25 下午9:11
 **/
@Data
public class UserDTO {

    /**
     * 用户唯一id
     */
    private String clientId;
    /**
     * email
     */
    private String email;
    /**
     * email激活状态
     */
    private Integer emailActive;
    /**
     * 电话
     */
    private String phoneNo;
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 头像
     */
    private String photo;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 性别
     */
    private String sex;
    /**
     * 用户登录状态
     */
    private String status;
    /**
     * 最后登录时间
     */
    private Date lastLoginDate;
    /**
     * 密码
     */
    private String password;
    /**
     * salt
     */
    private String salt;
}
