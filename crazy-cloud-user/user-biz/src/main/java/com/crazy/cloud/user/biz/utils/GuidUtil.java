package com.crazy.cloud.user.biz.utils;

import com.crazy.cloud.guid.api.GuidRequestDTO;
import com.crazy.cloud.guid.api.GuidService;
import java.util.Date;
import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * @program: crazy-cloud
 * @description: 获取全局唯一uuid
 * @author: CrazyMan
 * @create: 2018/9/28 上午11:07
 **/
public class GuidUtil {


    public static String nextId(GuidBizTypeEnum biz) {
        GuidService guidService = ApplicationContextUtil.getBean(GuidService.class);
        GuidRequestDTO guidRequestDTO = GuidRequestDTO.builder().bizType(biz.getBizType()).build();
        return DateFormatUtils.format(new Date(), "yyyyMMddHHmmss")
            + guidService.nextId(guidRequestDTO).getData().toString();
    }

    public enum GuidBizTypeEnum {
        CLIENT_ID("cloud.user.clientId");
        private final String bizType;

        GuidBizTypeEnum(String bizType) {
            this.bizType = bizType;
        }

        public String getBizType() {
            return this.bizType;
        }
    }
}
