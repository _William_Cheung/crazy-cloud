package com.crazy.cloud.user.domain.repository;

import com.crazy.cloud.user.dal.dataobject.UserLoginDO;
import com.crazy.cloud.user.dal.mapper.UserLoginMapper;
import com.crazy.cloud.user.domain.model.UserLogin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

/**
 * @program: crazy-cloud
 * @description: 用户登录
 * @author: CrazyMan
 * @create: 2018/9/26 下午4:08
 **/
@Slf4j
@Repository
public class UserLoginRepository extends BaseRepository<UserLoginMapper, UserLoginDO, UserLogin> {

    @Override
    protected String getEntityIdColumn() {
        return "clientId";
    }
}
