package com.crazy.cloud.user.domain.repository;

import com.crazy.cloud.user.dal.dataobject.UserDO;
import com.crazy.cloud.user.dal.mapper.UserMapper;
import com.crazy.cloud.user.domain.model.User;
import org.springframework.stereotype.Repository;

/**
 * @program: crazy-cloud
 * @description: 用户repository
 * @author: CrazyMan
 * @create: 2018/9/26 上午11:22
 **/
@Repository
public class UserRepository extends BaseRepository<UserMapper, UserDO, User> {


    public User selectUserByPhoneNo(String phoneNo) {
        return convert(getBaseMapper().selectOne(UserDO.builder().phoneNo(phoneNo).build()));
    }

    public User selectUserByEmail(String email) {
        return convert(getBaseMapper().selectOne(UserDO.builder().email(email).build()));
    }

    public User selectUserByUsername(String username) {
        return convert(getBaseMapper().selectOne(UserDO.builder().username(username).build()));
    }


    @Override
    protected String getEntityIdColumn() {
        return "clientId";
    }
}
