package com.crazy.cloud.common.utils;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

@Slf4j
public final class ConvertUtils {

    private ConvertUtils() {
    }

    public static <T> T convert(Object source, Class<T> targetClass) {
        if (source == null) {
            return null;
        }
        try {
            T target = targetClass.newInstance();
            BeanUtils.copyProperties(source, target);
            log.debug("convert {}:{}to {}:{}", source.toString(), source.hashCode(), target.toString(), target.hashCode());
            return target;
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("error happened when converting", e);
        }
        return null;
    }

    public static <T> List<T> convert(List source, Class<T> clazz) {
        ArrayList<T> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(source)) {
            return result;
        }
        for (Object s : source) {
            result.add(convert(s, clazz));
        }
        return result;
    }
}
