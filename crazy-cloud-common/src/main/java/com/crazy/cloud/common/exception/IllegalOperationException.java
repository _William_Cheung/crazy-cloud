package com.crazy.cloud.common.exception;

/**
 * @program: crazy-cloud
 * @description: 非法操作异常
 * @author: CrazyMan
 * @create: 2018/9/25 下午4:36
 **/
public class IllegalOperationException extends RuntimeException {

    public IllegalOperationException(String message) {
        super(message);
    }
}
