package com.crazy.cloud.common.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @program: crazy-cloud
 * @description: SaltUtil
 * @author: CrazyMan
 * @create: 2018/9/26 下午2:31
 **/
public class SaltUtil {

    private static int DEFAULT_BYTE_SIZE = 32;

    public static String getSalt() {
        return getSalt( DEFAULT_BYTE_SIZE );
    }
    
    public static String getSalt( int byteLen ) {
        return RandomStringUtils.randomAlphanumeric(byteLen);
    }
    
}
