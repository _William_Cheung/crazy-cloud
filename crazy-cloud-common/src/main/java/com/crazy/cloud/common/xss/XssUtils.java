package com.crazy.cloud.common.xss;

/**
 * @program: crazy-cloud
 * @description: xss工具
 * @author: CrazyMan
 * @create: 2018/9/27 下午8:15
 **/
public class XssUtils {

    /**
     * 清除转义字符
     */
    public static String clearXss(String value) {

        if (value == null || "".equals(value)) {
            return value;
        }
        value = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
        value = value.replaceAll("\\(", "&#40;").replace("\\)", "&#41;");
        value = value.replaceAll("'", "&#39;");
        value = value.replaceAll("eval\\((.*)\\)", "");
        value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']",
            "\"\"");
        value = value.replace("script", "");

        return value;
    }
}
