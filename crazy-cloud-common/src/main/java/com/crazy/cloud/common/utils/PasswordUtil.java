package com.crazy.cloud.common.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @program: crazy-cloud
 * @description: PasswordUtil
 * @author: CrazyMan
 * @create: 2018/9/26 下午2:31
 **/
public class PasswordUtil {

    private PasswordUtil() {
    }

    public static String encryptSha256(final String password, String salt) {
        if (StringUtils.isBlank(password)) {
            throw new NullPointerException("PasswordUtil#encrypt_sha256()传入参数为空");
        }
        return SHAUtil.sha256Hex(password + salt);
    }

    public static boolean loginPass(String currInputPsd, String psdEncryption, String salt) {
        String currInputPsdEncryption = encryptSha256(currInputPsd, salt);
        return currInputPsdEncryption.equals(psdEncryption);
    }

}
