package com.crazy.cloud.guid.biz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.crazy.cloud.guid"})
public class GuidBizApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuidBizApplication.class, args);
    }
}
