package com.crazy.cloud.guid.api;

import com.crazy.cloud.common.api.ApiResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("guid-service")
public interface GuidService {


    /**
     * 格式：自增序列
     */
    @PostMapping("/guid")
    ApiResponse<Long> nextId(@RequestBody GuidRequestDTO request);

}
