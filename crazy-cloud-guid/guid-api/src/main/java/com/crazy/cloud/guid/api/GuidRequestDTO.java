package com.crazy.cloud.guid.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GuidRequestDTO implements java.io.Serializable {

    private String bizType;
}
